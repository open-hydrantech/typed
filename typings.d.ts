declare module '*.json' {
	const value: any
	export default value
}
declare module '*.png'
declare module '*.jpg'

declare module 'react-native-meteor'
declare module 'react-native-chooser'
declare module 'react-native-camera'
declare module 'tcomb-form-native'
declare module 'react-native-restart'
