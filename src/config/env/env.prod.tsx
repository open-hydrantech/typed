export default {
	APP_NAME: 'HydrantUp',
	SERVER_URL: 'wss://app.hydrant-system.com/websocket',
};
