import React from 'react'
import { View, TextInput } from 'react-native'
import styles from './styles'

export const GenericTextInput = (props: AnyObject) => (
	<View>
		{props.borderTop && <View style={styles.divider} />}
		<TextInput style={styles.input} autoCapitalize="none" autoCorrect={false} {...props} />
	</View>
)
