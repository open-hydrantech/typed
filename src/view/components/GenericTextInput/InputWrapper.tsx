import React from 'react'
import { View } from 'react-native'
import styles from './styles'

export const InputWrapper = (props: AnyObject) => <View style={styles.inputWrapper}>{props.children}</View>
