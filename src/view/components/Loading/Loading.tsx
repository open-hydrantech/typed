import { Text, StyleSheet, View, ActivityIndicator } from 'react-native'
import React from 'react'
import _ from 'lodash'
import { inject, observer } from 'mobx-react/native'
import { observable, action, computed } from 'mobx'
import Spinner from 'react-native-loading-spinner-overlay'

@observer
export class Loading extends React.Component<{ timeout?: number; loading?: boolean }> {
	spinnerInterval: number
	mounted = false
	@observable spinner = 3
	@observable spinnerInc = 1
	@observable timeout = this.props.timeout || 0
	@action set = (obj: object) => _.assign(this, obj)

	intSpinner() {
		if (!this.mounted) return
		console.log('this.timeout')
		console.log(this.timeout)
		if (this.timeout && this.timeout > 0) this.set({ timeout: this.timeout - 1 })
		if (!this.timeout) clearInterval(this.spinnerInterval)
		const { spinner } = this
		let inc = this.spinnerInc
		this.set({ spinner: spinner + inc })
		if (spinner > 10) this.set({ spinnerInc: -1 })
		else if (spinner <= 4) this.set({ spinnerInc: 1 })
	}
	componentDidMount() {
		this.mounted = true
		console.log('loading mounted')
		if (this.props.loading && this.props.timeout && !this.spinnerInterval)
			this.spinnerInterval = setInterval(() => this.intSpinner(), 1 * 1000)
	}
	componentWillReceiveProps() {
		if (this.props.loading && this.props.timeout) {
			if (!this.spinnerInterval) this.spinnerInterval = setInterval(() => this.intSpinner(), 1 * 1000)
		} else {
			clearInterval(this.spinnerInterval)
		}
	}
	componentWillUnmount() {
		this.mounted = false
		console.log('loading UNmounted')
		clearInterval(this.spinnerInterval)
	}
	render() {
		const spinner = _.repeat('.', this.spinner)
		const visible = this.props.loading && ((this.props.timeout && !!this.timeout) || !this.props.timeout)
		return (
			<View>
				<Spinner {...{ visible }} textContent={`${spinner}טוען`} />
			</View>
		)
	}
}
