import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import _ from 'lodash'
import { useStrict, observable, action, computed } from 'mobx'
import { inject, observer } from 'mobx-react/native'

import { Button } from 'view/components/Button/Button'
import { GenericTextInput } from 'view/components/GenericTextInput/GenericTextInput'
import { InputWrapper } from 'view/components/GenericTextInput/InputWrapper'
import logoImage from 'assets/logo.jpg'
import { Loading } from 'view/components/Loading/Loading'

import styles from './styles'

import { Store } from 'models/Store'

useStrict(true)

@observer
export class Login extends Component {
	// static navigationOptions = ({ navigation }) => ({
	// 	title: navigation.state.params && navigation.state.params.title || 'Login',
	// });
	@observable email = ''
	@observable password = ''
	@observable confirmPassword = ''
	@observable confirmPasswordVisible = false
	@observable error = null
	@observable loading = false

	@action
	set = (obj: AnyObject) => {
		if (obj.password || obj.email) obj.error = ''
		return _.assign(this, obj)
	}

	@action handleError = (error: string) => this.set({ error })

	validInput(overrideConfirm: boolean) {
		const { email, password, confirmPassword, confirmPasswordVisible } = this

		let valid = true

		if (email.length === 0 || password.length === 0) {
			this.handleError('אימייל או סיסמה ריקים')
			valid = false
		}

		if (!overrideConfirm && confirmPasswordVisible && password !== confirmPassword) {
			this.handleError('אימייל או סיסמה ריקים')
			valid = false
		}

		if (valid) {
			this.handleError('')
		}

		return valid
	}

	@action
	handleSignIn = async () => {
		if (this.validInput(true)) {
			const { email, password } = this
			this.set({ loading: true })
			const err = await Store!.app!.login(email, password)
			if (err) {
				this.set({ loading: false })
				this.handleError(err)
			}
		}
	}

	render() {
		console.log('rendering login')
		const { error } = this

		return (
			<View style={styles.container}>
				<Loading loading={this.loading} />
				<View style={styles.header}>
					<Image style={styles.logo} source={logoImage} />
				</View>

				<InputWrapper>
					<GenericTextInput placeholder="Email" onChangeText={(email: string) => this.set({ email })} />
					<GenericTextInput
						placeholder="Password"
						onChangeText={(password: string) => this.set({ password })}
						secureTextEntry
					/>
				</InputWrapper>

				<View style={styles.error}>
					<Text style={styles.errorText}>{error}</Text>
				</View>

				<View style={styles.buttons}>
					<Button text="היכנס" onPress={this.handleSignIn} />
				</View>
			</View>
		)
	}
}

// componentWillMount() {
// 	this.mounted = true;
// }
//
// componentWillUnmount() {
// 	this.mounted = false;
// }
//
// handleError(error) {
// 	if (this.mounted) {
// 		this.setState({ error });
// 	}
// }
