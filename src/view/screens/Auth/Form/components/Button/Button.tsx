import React from 'react'
import { StyleProp, StyleSheet, Text, TouchableHighlight, TouchableOpacity } from 'react-native'
import { inject, observer } from 'mobx-react/native'

import styles from './styles'

interface Props extends AnyObject {
	text: string
	onPress: () => void
	style?: StyleProp<any>
}
export const Button = observer(({ text, style = {}, onPress, disabled = false }: Props) => {
	return (
		<TouchableOpacity
			{...{ onPress, disabled }}
			style={[styles.button, style, disabled && { backgroundColor: 'lightgray' }]}
			// underlayColor="#99d9f4"
		>
			<Text style={styles.buttonText}>{text}</Text>
		</TouchableOpacity>
	)
})
