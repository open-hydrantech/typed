import React, { Component } from 'react'
import _ from 'lodash'
import { Dimensions, TextInput, ScrollView, Text, View } from 'react-native'
import { inject, observer } from 'mobx-react/native'
import DatePicker from 'react-native-datepicker'

import { FormModel } from 'models/Form/Form'
import { Store } from 'models/Store'

import styles from './styles'

export const PickBatchDate = observer(() => {
	const { errorStatus, batchDate, onBatchDateSelect } = Store!.form as typeof FormModel.Type
	return (
		<DatePicker
			style={{ width: Dimensions.get('window').width - 40 }}
			date={batchDate}
			onDateChange={date => onBatchDateSelect(date)}
			mode="date"
			androidMode="spinner"
			placeholder="בחר"
			format="DD/MM/YYYY"
			minDate="01/01/2000"
			customStyles={{
				dateText: styles.dateText,
				placeholderText: styles.placeholderText,
				dateIcon: styles.dateIcon,
				dateInput: [styles.dateInput, errorStatus && !batchDate && { borderColor: 'red' }],
			}}
		/>
	)
})
