import React, { Component } from 'react'
import _ from 'lodash'
import { Dimensions, TextInput, ScrollView, Text, View } from 'react-native'
import { inject, observer } from 'mobx-react/native'
import { Select, Option } from 'react-native-chooser'

import { FormModel } from 'models/Form/Form'
import { CompaniesModel } from 'models/Form/Companies'
import { Store } from 'models/Store'

import styles from './styles'

export const SelectCompany = observer(() => {
	const { errorStatus } = Store!.form as typeof FormModel.Type
	const { items, selectedId, onSelectItem } = Store!.form!.companies as typeof CompaniesModel.Type
	return (
		<Select
			indicator="down"
			indicatorColor="gray"
			textStyle={styles.selectText}
			transparent
			style={[styles.selectBox, errorStatus && !selectedId && { borderColor: 'red' }]}
			onSelect={(value: string) => onSelectItem(value)}
			defaultText="בחר"
			selected={selectedId}
			optionListStyle={styles.optionList}
		>
			{items.map(el => (
				<Option key={el._id} value={el._id} style={styles.option} styleText={styles.optionText}>
					{el.name}
				</Option>
			))}
		</Select>
	)
})
