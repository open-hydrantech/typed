import React, { Component } from 'react'
import _ from 'lodash'
import { observer } from 'mobx-react/native'
import { TouchableOpacity, FlatList, View, Text } from 'react-native'

import styles from './styles'
import Separator from '../../components/Separator'

import { AdModel } from 'models/Form/Dialog/Ad'
import { Store } from 'models/Store'

export const Item = observer(({ index }) => {
	const { ads, onSelect, selectedIndex } = Store!.form!.ad as typeof AdModel.Type
	const item = ads[index]
	return (
		<TouchableOpacity onPress={() => onSelect(index)}>
			<View>
				<Text style={selectedIndex === index ? styles.listItemSelectedText : styles.listItemNotSelectedText}>
					{item.ad}
				</Text>
			</View>
		</TouchableOpacity>
	)
})
