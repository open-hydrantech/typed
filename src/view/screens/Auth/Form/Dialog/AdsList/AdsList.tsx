import React, { Component } from 'react'
import _ from 'lodash'
import { observer } from 'mobx-react/native'
import { TouchableOpacity, FlatList, View, Text } from 'react-native'

import styles from './styles'
import { Separator } from '../../components/Separator'
import { AdModel } from 'models/Form/Dialog/Ad'
import { Store } from 'models/Store'
import { Item } from './Item'

export const AdsList = observer(() => {
	const { ads } = Store!.form!.ad as typeof AdModel.Type
	return (
		<View style={{ marginTop: 10, flex: 1 }}>
			<Text style={styles.listTitleText}>בחר</Text>
			<FlatList
				data={ads}
				ItemSeparatorComponent={() => Separator({ style: { height: 2, backgroundColor: '#fff' } })}
				keyExtractor={(item, index) => item.id}
				renderItem={({ item, index }) => <Item {...{ index }} />}
			/>
		</View>
	)
})
