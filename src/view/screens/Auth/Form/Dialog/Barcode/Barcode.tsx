import React, { Component } from 'react'
import _ from 'lodash'
import { observer } from 'mobx-react/native'
import { View, Text } from 'react-native'
import Camera from 'react-native-camera'

import { BarcodeModel } from 'models/Form/Dialog/Barcode'
import { Store } from 'models/Store'

import styles from './styles'
import { OptionalValue } from 'mobx-state-tree/dist/internal'

export const Barcode = observer(() => {
	const { onBarCodeRead, value } = Store!.form!.barcode as typeof BarcodeModel.Type
	return (
		<View style={{ flex: 1 }}>
			<Camera
				style={styles.camera}
				onBarCodeRead={(barcode: AnyObject) => onBarCodeRead(barcode)}
				aspect={Camera.constants.Aspect.fill}
			>
				<Text style={styles.cameraText}>{value}</Text>
			</Camera>
		</View>
	)
})

//
// onBarCodeRead = (e) => {
// 	console.log(
// 		'Barcode Found!',
// 		'Type: ' + e.type + '\nData: ' + e.data
// 	);
// }
// ref={cam => this.camera = cam}
// style={styles.preview}
