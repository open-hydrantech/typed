import React, { Component } from 'react'
import _ from 'lodash'
import { observer } from 'mobx-react/native'
import { View, Text } from 'react-native'

import { LocationModel } from 'models/Form/Dialog/Location'
import { Store } from 'models/Store'

import styles from './styles'

export const Location = observer(() => {
	const { location } = Store!.form!.location as typeof LocationModel.Type

	return (
		<View style={{ marginTop: 20, flex: 1 }}>
			<Text style={styles.locationText}>דיוק: {Math.round(_.get(location, 'coords.accuracy'))} מטר </Text>
			<Text style={styles.locationText}>קו רוחב: {_.get(location, 'coords.latitude')}</Text>
			<Text style={styles.locationText}>קו אורך: {_.get(location, 'coords.longitude')}</Text>
		</View>
	)
})
