import React from 'react'
import { View, Text } from 'react-native'
import { observer } from 'mobx-react/native'
import Spinner from 'react-native-loading-spinner-overlay'
import { Col, Row, Grid } from 'react-native-easy-grid'

import { Button } from '../components/Button/Button'
import { Barcode } from './Barcode/Barcode'
import { Location } from './Location/Location'
import { AdsList } from './AdsList/AdsList'

import { DialogModel } from 'models/Form/Dialog/Dialog'
import { Store } from 'models/Store'

import styles from './styles'

const componentOptions: { [key: string]: () => JSX.Element } = { Barcode, Location, AdsList }

export const Dialog = observer(() => {
	const { component, onOk, onCancel } = Store!.form!.dialog as typeof DialogModel.Type

	return (
		<Spinner visible>
			<View style={styles.overlay}>
				<Grid>
					<Row>{React.createElement(componentOptions[component])}</Row>
					<Row style={{ marginTop: 30, height: 50 }}>
						<Col>
							<Button onPress={() => onOk()} text="אשר" />
						</Col>
						<Col style={{ width: 20 }} />
						<Col>
							<Button onPress={() => onCancel()} text="בטל" />
						</Col>
					</Row>
				</Grid>
			</View>
		</Spinner>
	)
})
