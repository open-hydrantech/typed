import React, { Component } from 'react'
import _ from 'lodash'
import { Dimensions, TextInput, ScrollView, Text, View } from 'react-native'
import { inject, observer } from 'mobx-react/native'
import Spinner from 'react-native-loading-spinner-overlay'

import { Dialog } from './Dialog/Dialog'
import { Alert } from './components/Alert/Alert'

import { Button } from './components/Button/Button'
import { Separator } from './components/Separator'

import { tcombForm } from './TcombForm/TcombForm'
import { SelectCompany } from './components/SelectCompany/SelectCompany'
import { PickBatchDate } from './components/PickBatchDate/PickBatchDate'

import { Store } from 'models/Store'
import { FormModel } from 'models/Form/Form'

import styles from './styles'

export const Form = observer(() => {
	const FormStore = Store!.form as typeof FormModel.Type
	console.log('rendering Form', FormStore.formValue)
	const { TCombForm, formStruct, formOptions } = tcombForm()
	return (
		<ScrollView contentContainerStyle={styles.container}>
			<Spinner {...{ visible: FormStore.loading }} textContent="טוען..." />
			{FormStore.dialog.visible && !FormStore.loading && <Dialog />}
			{FormStore.alert.visible && !FormStore.loading && <Alert />}
			{!FormStore.connected && <Text style={styles.notConnected}>מנסה להתחבר</Text>}
			<Button onPress={() => FormStore.onBarcodeOpen('sim')} text="ברקוד סים" />
			<Button onPress={() => FormStore.onBarcodeOpen('bodyBarcode')} text="ברקוד גוף" />
			<Button onPress={() => FormStore.onAdOpen()} disabled={FormStore.adButtonDisabled} text="כתובת" />
			<Button onPress={() => FormStore.onLocationOpen()} text="מיקום" />

			<TCombForm
				ref={(ref: object) => FormStore.setFormRef(ref)}
				type={formStruct}
				options={formOptions}
				value={FormStore.formValue}
				onChange={(value: object) => FormStore.onFormChange(value)}
			/>

			<Text style={styles.label}>חברה</Text>
			<SelectCompany />

			<Separator style={{ height: 15 }} />

			<Text style={styles.label}>תאריך ייצור</Text>
			<PickBatchDate />

			<Separator style={{ height: 35 }} />

			<Button
				style={FormStore.submitButtonRed && { borderColor: 'red', borderWidth: 3 }}
				onPress={() => FormStore.onSubmit()}
				text="שמור"
			/>
		</ScrollView>
	)
})
