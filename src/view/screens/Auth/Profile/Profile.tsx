import React, { Component } from 'react'
import _ from 'lodash'
import { Button } from 'react-native-elements'
import RNRestart from 'react-native-restart'
import { InteractionManager, TouchableOpacity, StyleSheet, Text, View } from 'react-native'
import { inject, observer } from 'mobx-react/native'
import { useStrict, observable, action, computed } from 'mobx'
import { Col, Row, Grid } from 'react-native-easy-grid'

import Loading from '../../../components/Loading/Loading'
import { deferRun, sleep } from '../../../../lib/Utils'

import styles from './styles'

import { Store } from 'models/Store'

useStrict(true)

@observer
export class Profile extends Component<any> {
	render() {
		console.log('profile')
		console.log(this.props)
		const { props } = this
		const { navigation } = props
		const { navigate } = navigation
		const user = Store!.app!.user
		const { name, email, companyName } = user as any
		return (
			<View
				style={{
					backgroundColor: 'aliceblue',
					flex: 1,
					flexDirection: 'column',
					justifyContent: 'space-between',
					alignItems: 'stretch',
				}}
			>
				<View
					style={{
						paddingTop: 5,
						height: 7,
						flexDirection: 'column',
						justifyContent: 'space-between',
						alignItems: 'center',
					}}
				>
					<Text style={styles.nameBold}>שם</Text>
				</View>
				<View style={{ height: 7, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
					<Text style={styles.name}>{name}</Text>
				</View>
				<View
					style={{
						paddingTop: 15,
						height: 7,
						flexDirection: 'column',
						justifyContent: 'space-between',
						alignItems: 'center',
					}}
				>
					<Text style={styles.nameBold}>אימייל</Text>
				</View>
				<View style={{ height: 7, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
					<Text style={styles.name}>{email}</Text>
				</View>
				<View
					style={{
						paddingTop: 15,
						height: 7,
						flexDirection: 'column',
						justifyContent: 'space-between',
						alignItems: 'center',
					}}
				>
					<Text style={styles.nameBold}>חברה</Text>
				</View>
				<View style={{ height: 7, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
					<Text style={styles.name}>{companyName}</Text>
				</View>

				<View
					style={{
						margin: 20,
						height: 40,
						flexDirection: 'column',
						justifyContent: 'space-between',
						alignItems: 'center',
					}}
				>
					<Button
						component={TouchableOpacity}
						raised
						icon={{ name: 'log-out', type: 'feather', size: 32 }}
						buttonStyle={{ width: 150, backgroundColor: 'lightskyblue', borderRadius: 0 }}
						textStyle={styles.main}
						title="לצאת"
						onPress={() => Store!.app!.logout()}
					/>
				</View>
				<TouchableOpacity onPress={() => RNRestart.Restart()} style={{ height: 50, backgroundColor: 'powderblue' }} />
			</View>
		)
	}
}

//
// <Grid>
// 	<Row size={1}>
// 		<Col size={1}>
// 			<Row style={{ height: 40 }}>
// 				<View style={{ paddingTop: 10, height: 7, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
// 					<Text style={styles.nameBold}>שם</Text>
// 				</View>
// 			</Row>
// 			<Row style={{ height: 40 }}>
// 				<View style={{ height: 7, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
// 					<Text style={styles.name}>{name}</Text>
// 				</View>
// 			</Row>
// 		</Col>
// 		<Col size={1}>
// 			<Text>12</Text>
// 		</Col>
// 		<Col size={1}>
// 			<Row style={{ height: 40 }}>
// 				<View style={{ paddingTop: 10, height: 7, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
// 					<Text style={styles.nameBold}>אימייל</Text>
// 				</View>
// 			</Row>
// 			<Row style={{ height: 40 }}>
// 				<View style={{ height: 7, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
// 					<Text style={styles.name}>{email}</Text>
// 				</View>
// 			</Row>
// 		</Col>
// 	</Row>
// 	<Row size={1}>
// 		<Col size={1}>
// 			<Text>21</Text>
// 		</Col>
// 		<Col size={1}>
// 			<Text>22</Text>
// 		</Col>
// 		<Col size={1}>
// 			<Text>23</Text>
// 		</Col>
// 	</Row>
// 	<Row size={1}>
// 		<Col size={1}>
// 			<Text>31</Text>
// 		</Col>
// 		<Col size={1}>
// 			<Row style={{ height: 40 }}>
// 				<View style={{ paddingTop: 10, height: 7, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
// 					<Text style={styles.nameBold}>חברה</Text>
// 				</View>
// 			</Row>
// 			<Row style={{ height: 40 }}>
// 				<View style={{ height: 7, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
// 					<Text style={styles.name}>{companyName}</Text>
// 				</View>
// 			</Row>
// 		</Col>
// 		<Col size={1}>
// 			<Text>33</Text>
// 		</Col>
// 	</Row>
// </Grid>
