import React from 'react'
import { I18nManager, InteractionManager } from 'react-native'
import _ from 'lodash'
import { reaction, when, useStrict, observable, action, computed } from 'mobx'
import { getRoot, flow, types, getParent } from 'mobx-state-tree'

import { Base } from 'models/Base'

import Settings from '../config/settings'
import * as u from '../lib/Utils'

useStrict(true)

const User = types.model('User', {
	name: '',
	email: '',
	companyName: '',
	userId: '',
})

const Model = types.model({
	lastComm: new Date(0),
	connected: true,
	fcmToken: '',
	user: types.maybe(User),
})

export const AppModel = types
	.compose(Base, Model)
	.named('AppModel')
	.views(me => ({
		get auth() {
			console.log('in auth')
			return u.isNotEmptyString(_.get(me, 'user.userId'))
		},
	}))
	.actions(me => {
		let timeOutCounter: string
		const autoConnect = flow(function* autoConnect() {
			console.log('connecting')
			const { data: { status } } = yield u.toto(u.meteorCall('mobile.connect'), 25000)
			if (status === 'OK') {
				me.set({ connected: true, lastComm: new Date() })
			} else if (timeOutCounter === 'deems to be not connected') {
				me.set({ connected: false })
				yield u.Server.reset()
				timeOutCounter = ''
			}
			timeOutCounter = 'deems to be not connected'
		})
		return { autoConnect }
	})
	.actions(me => ({
		afterAttach: flow(function* afterCreate() {
			console.log('app here')
			u.Server.connect()
			u.interval(me.autoConnect, 10000)

			const user = yield u.LocalStorage.get('user')
			me.user = User.create(user || {})
			console.log('user', me.user)
			reaction(
				() => me.auth,
				auth => {
					if (auth) {
						console.log('creating form')
						getRoot(me).createForm()
						me.Store.nav.navigate('Auth')
					} else {
						getRoot(me).restart()
						me.Store.nav.navigate('Login')
					}
				},
				{ fireImmediately: true },
			)
		}),
		logout() {
			console.log('logout')
			const { user } = me
			u.toto(u.meteorCall('mobile.logout', { user }), 15000)
			me.set({ user: {} })
			u.LocalStorage.delete('user')
		},
		login: flow(function* login(email: string, password: string) {
			console.log('login')

			const { err } = yield u.toto(u.meteorLoginWithPassword(email, password))
			if (err) return 'אימייל או סיסמא אינם מתאימים'

			const { data: { user } } = yield u.toto(
				u.meteorCall('mobile.login', {
					deviceInfo: u.getDeviceInfo(),
					appName: Settings.APP_NAME,
				}),
				15000,
			)
			console.log('user', user)
			if (typeof user !== 'object' || (Settings.APP_NAME == 'HydrantUp' && user.role != 'admin'))
				return 'אימייל או סיסמא אינם מתאימים'

			me.user = user
			yield u.LocalStorage.save('user', user)
			me.Store.nav.navigate('Login')
			return true
		}),
		setFcmToken(fcmToken: string) {
			console.log('sync', 'fcmToken', fcmToken)
			me.set({ fcmToken })
			const { user } = me
			u.toto(u.meteorCall('mobile.set.fcmtoken', { user, fcmToken }), 15000)
		},
	}))

// reaction(
// 	() => me.auth,
// 	async auth => {
// 		if (auth) me.autoConnect('start')
// 		else me.autoConnect('stop')
// 	},
// )
