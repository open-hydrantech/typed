import React, { Component } from 'react'
import { InteractionManager, Dimensions, StyleSheet } from 'react-native'
import _ from 'lodash'
import axios from 'axios'
import { when, useStrict, observable, action, computed } from 'mobx'
import { destroy, getRoot, flow, types, getParent } from 'mobx-state-tree'

import * as u from 'lib/Utils'

import LocationDialog from 'view/screens/Auth/Form/Dialog/Location/Location'
import BarcodeDialog from 'view/screens/Auth/Form/Dialog/Barcode/Barcode'
import AdDialog from 'view/screens/Auth/Form/Dialog/AdsList/AdsList'

import { AdModel } from './Dialog/Ad'
import { LocationModel } from './Dialog/Location'
import { BarcodeModel } from './Dialog/Barcode'

import { Base } from 'models/Base'
import { CompaniesModel } from './Companies'
import { AlertModel } from './Alert'
import { DialogModel } from './Dialog/Dialog'

useStrict(true)

const Model = types.model({
	loading: true,

	alert: types.optional(AlertModel, {}),
	dialog: types.optional(DialogModel, {}),
	ad: types.maybe(AdModel),
	location: types.maybe(LocationModel),
	barcode: types.maybe(BarcodeModel),

	batchDate: '',
	companies: types.optional(CompaniesModel, {}),
	formValue: types.optional(types.frozen, {}),
	errorStatus: false,
	submitButtonRed: false,
})

export const FormModel = types
	.compose(Base, Model)
	.named('FormModel')
	.views(me => ({
		get locationAvailable() {
			return me.formValue.lat && me.formValue.lat
		},
	}))
	.views(me => ({
		get connected() {
			return getRoot(me).app.connected
		},
		get adButtonDisabled() {
			return !me.locationAvailable
		},
	}))
	.actions(me => ({
		setLoading(loading: boolean) {
			me.loading = loading
		},
	}))
	.actions(me => {
		function afterAttach() {
			console.log(11)
			when(() => me.companies.ready, () => me.setLoading(false))
		}
		let formRef: AnyObject
		//
		async function onBarcodeOpen(field: string) {
			me.barcode = BarcodeModel.create()
			if (await me.dialog.show(me.barcode)) {
				me.formValue[`${field}`] = me.barcode.value
			}
			destroy(me.barcode)
		}
		async function onLocationOpen() {
			me.location = LocationModel.create()
			if (await me.dialog.show(me.location)) {
				me.formValue.lat = me.location.lat
				me.formValue.lon = me.location.lon
			}
			destroy(me.location)
		}
		async function onAdOpen() {
			if (!me.locationAvailable()) return
			const { lat, lon } = me.formValue
			me.ad = AdModel.create({}, { lat, lon })
			if (await me.dialog.show(me.ad)) {
				me.formValue.address = me.ad.selected
			}
			destroy(me.ad)
		}
		//
		async function onSubmit() {
			const formValue = formRef.getValue()
			if (!formValue) {
				me.errorStatus = true
				InteractionManager.runAfterInteractions(async () => {
					for (let i = 0; i < 3; i += 1) {
						me.set({ submitButtonRed: true })
						await u.sleep(1500)
						me.set({ submitButtonRed: false })
						await u.sleep(1200)
					}
				})
			} else {
				const { lat, lon, address, sim, bodyBarcode, description, remarks, history } = formValue
				const { selectedId: companyId } = me.companies
				const { batchDate } = me
				const lastComm = new Date()
				const doc = {
					lastComm,
					lat,
					lon,
					address,
					sim,
					bodyBarcode,
					description,
					remarks,
					history,
					batchDate,
					companyId,
				}
				//
				me.set({ loading: true })
				const { user } = me.Store.app
				const { data } = await u.toto(u.meteorCall('mobile.hydrant.insert', { user, doc }), 15000)
				me.set({ loading: false })
				//
				if (data) {
					me.set({ formValue: {}, batchDate: '' })
					me.companies.set({ companyId: '' })
					me.alert.show('השליחה הצליחה!', `מספר הידרנט: ${data.number}`)
				} else {
					me.alert.show('השליחה לא הצליחה')
				}
			}
		}
		return {
			afterAttach,
			onSubmit,
			onBarcodeOpen,
			onLocationOpen,
			onAdOpen,
			setFormRef: (ref: object) => (formRef = ref),
			onFormChange: (formValue: object) => me.set({ formValue }),
			onBatchDateSelect: (batchDate: string) => me.set({ batchDate }),
		}
	})

// ad: types.optional(Ad, {}),
// location: types.optional(Location, {}),
// barcode: types.optional(Barcode, {}),
