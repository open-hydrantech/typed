import React, { Component } from 'react'
import { InteractionManager, Dimensions, StyleSheet } from 'react-native'
import _ from 'lodash'
import axios from 'axios'
import { when, useStrict, observable, action, computed } from 'mobx'
import { getRoot, flow, types, getParent } from 'mobx-state-tree'

import * as u from 'lib/Utils'

import { Base } from 'models/Base'

useStrict(true)

const Model = types.model({
	visible: false,
	title: '',
	text: '',
})

export const AlertModel = types
	.compose(Base, Model)
	.named('Alert')
	.views(me => ({}))
	.actions(me => {
		const onOk = () => (me.visible = false)
		function show(title: string, text = '') {
			me.set({ title, text })
			me.visible = true
			return new Promise(resolve => {
				while (me.visible) {}
				resolve()
			})
		}
		return { show, onOk }
	})
