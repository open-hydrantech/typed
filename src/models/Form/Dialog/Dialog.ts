import React, { Component } from 'react'
import { InteractionManager, Dimensions, StyleSheet } from 'react-native'
import _ from 'lodash'
import axios from 'axios'
import { when, useStrict, observable, action, computed } from 'mobx'
import { IModelType, getRoot, flow, types, getParent } from 'mobx-state-tree'

import * as u from 'lib/Utils'

import { Base } from 'models/Base'
useStrict(true)

const Model = types.model({
	visible: false,
	component: '',
})

export const DialogModel = types
	.compose(Base, Model)
	.named('Dialog')
	.views(me => ({}))
	.actions(me => {
		let ok: boolean
		function show(model: { component: string }) {
			ok = false
			me.component = model.component
			me.visible = true
			return new Promise(resolve => {
				while (me.visible) {}
				ok ? resolve(true) : resolve(false)
			})
		}
		return {
			show,
			onOk: () => {
				me.visible = false
				ok = true
			},
			onCancel: () => {
				me.visible = false
				ok = false
			},
		}
	})
