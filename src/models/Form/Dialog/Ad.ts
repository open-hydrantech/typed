import React, { Component } from 'react'
import { InteractionManager, Dimensions, StyleSheet } from 'react-native'
import _ from 'lodash'
import axios from 'axios'
import { when, useStrict, observable, action, computed } from 'mobx'
import { getParent, getEnv, types } from 'mobx-state-tree'

import * as u from 'lib/Utils'
import { Base } from 'models/Base'

useStrict(true)

const AdItem = types.model({
	id: '',
	ad: '',
})

const Model = types.model({
	ads: types.optional(types.array(AdItem), []),
	selectedIndex: types.maybe(types.number),
	component: 'Ad',
})
export const AdModel = types
	.compose(Base, Model)
	.named('Ad')
	.views(me => ({
		get selected() {
			return _.get(me.ads, `[${me.selectedIndex}].ad`)
			// return _.find(me.ads, ['id', me.selectedId])
		},
		// get form() {
		// 	return getParent(me)
		// },
	}))
	.actions(me => {
		async function afterAttach() {
			me.set({ loading: true })
			for (let i = 0; i < 10; i += 1) {
				const { data, err, timeOut } = await u.toto(
					axios.get(
						`https://maps.googleapis.com/maps/api/geocode/json?language=iw&latlng=${getEnv(me).lat},${
							getEnv(me).lon
						}&AIzaSyBLZ9MQsAOpEzHcubQCo-fsKhb1EoUt88U`,
					),
					10000,
				)

				if (data && data.data && !_.isEmpty(data.data.results)) {
					_.forEach(data.data.results, el => me.ads.push({ id: el.place_id, ad: el.formatted_address }))
					break
				}
			}
			me.set({ loading: false })
		}
		return {
			afterAttach,
			onSelect: (selectedIndex: number) => me.set({ selectedIndex }),
		}
	})
