import React, { Component } from 'react'
import { InteractionManager, Dimensions, StyleSheet } from 'react-native'
import _ from 'lodash'
import axios from 'axios'
import { when, useStrict, observable, action, computed } from 'mobx'
import { flow, types, getParent } from 'mobx-state-tree'

import * as u from 'lib/Utils'

import { Base } from 'models/Base'
import Ad from 'view/screens/Auth/Form/Dialog/AdsList/AdsList'

useStrict(true)

const Model = types.model({
	location: types.optional(types.frozen, {}),
	component: 'Location',
})
export const LocationModel = types
	.compose(Base, Model)
	.named('Location')
	.views(me => ({
		get lat() {
			return _.get(me.location, 'coords.latitude')
		},
		get lon() {
			return _.get(me.location, 'coords.longitude')
		},
		get accuracy() {
			return _.get(me.location, 'coords.accuracy')
		},
	}))
	.actions(me => {
		let positionId: number
		async function setLocation(location: AnyObject) {
			if (!me.location || _.get(location, 'coords.accuracy') < _.get(me.location, 'coords.accuracy')) {
				me.set({ location })
			}
		}
		async function afterAttach() {
			u.requestLocationPermission()
			me.location = undefined
			me.set({ loading: true })
			when(() => me.location, () => me.set({ loading: false }))
			positionId = navigator.geolocation.watchPosition(l => setLocation(l), undefined, {
				useSignificantChanges: false,
				enableHighAccuracy: true,
				timeout: 75000,
				maximumAge: 2,
				distanceFilter: 0,
			})
		}
		async function beforeDestroy() {
			navigator.geolocation.clearWatch(positionId)
			navigator.geolocation.stopObserving()
		}
		return { afterAttach, beforeDestroy }
	})
