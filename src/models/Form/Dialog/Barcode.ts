import React, { Component } from 'react'
import { InteractionManager, Dimensions, StyleSheet } from 'react-native'
import _ from 'lodash'
import axios from 'axios'
import { when, useStrict, observable, action, computed } from 'mobx'
import { flow, types, getParent } from 'mobx-state-tree'

import * as u from 'lib/Utils'
import Ad from 'view/screens/Auth/Form/Dialog/AdsList/AdsList'
import { Base } from 'models/Base'

useStrict(true)

const Model = types.model({
	component: 'Barcode',
	value: '',
})
export const BarcodeModel = types
	.compose(Base, Model)
	.named('Barcode')
	.actions(me => ({
		onBarCodeRead: (data: AnyObject) => (me.value = data.data),
	}))
