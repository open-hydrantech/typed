import React, { Component } from 'react'
import { InteractionManager, Dimensions, StyleSheet } from 'react-native'
import _ from 'lodash'
import axios from 'axios'
import { when, useStrict, observable, action, computed } from 'mobx'
import { getRoot, flow, types, getParent } from 'mobx-state-tree'
import { Base } from '../Base'

import * as u from '../../lib/Utils'

useStrict(true)

const Model = types.model({
	items: types.optional(types.array(types.optional(types.frozen, {})), []),
	selectedId: '',
	ready: false,
})

export const CompaniesModel = types
	.compose(Base, Model)
	.named('Companies')
	.views(me => ({}))
	.actions(me => {
		const replaceItems = (items: AnyObject[]) => me.items.replace(items)
		async function loadItems() {
			console.log(1)
			console.log(me)
			console.log(getRoot(me))
			const { user } = getRoot(me).app
			console.log(2)
			const { data } = await u.toto(u.meteorCall('mobile.companies.get.all', { user }))
			replaceItems(data)
		}
		function afterAttach() {
			console.log('loading companies')
			console.log(getParent(me, 2))

			loadItems().then(() => (me.ready = true))
		}
		return { afterAttach, onSelectItem: (selectedId: string) => me.set({ selectedId }) }
	})
