import React from 'react'
import _ from 'lodash'
import { reaction, when, useStrict, observable, action, computed } from 'mobx'
import { flow, types, getParent, destroy } from 'mobx-state-tree'

import { NavModel } from './Nav'
import { AppModel } from './App'
import { FormModel } from './Form/Form'

import Settings from '../config/settings'
import * as u from '../lib/Utils'

useStrict(true)

export const Store = types
	.model({
		app: types.maybe(AppModel),
		nav: types.optional(NavModel, {}),
		form: types.optional(FormModel, {}),
	})
	.named('Store')
	.actions(me => ({
		createForm() {
			console.log('creating form')
			// me.form = FormModel.create({})
		},
		destroyForm() {
			me.form && destroy(me.form)
		},
		restart() {
			console.log('destroying app')
			me.form && destroy(me.form)
			me.app && destroy(me.app)
		},
	}))
	.actions(me => ({
		afterCreate() {
			me.app = AppModel.create({})
			console.log('creating app')
		},
	}))
	.create({})
