import { NavigationActions } from 'react-navigation'
import _ from 'lodash'
import {
	intercept,
	extendObservable,
	observe,
	autorun,
	reaction,
	when,
	toJS,
	useStrict,
	observable,
	action,
	computed,
} from 'mobx'
import { flow, types, getParent } from 'mobx-state-tree'

import { Base } from 'models/Base'

import Settings from '../config/settings'
import * as u from '../lib/Utils'

useStrict(true)

const Model = types.model({
	appNavigation: types.optional(types.frozen, {}),
	authNavigation: types.optional(types.frozen, {}),
})
export const NavModel = types
	.compose(Base, Model)
	.named('NavModel')
	.actions(me => ({
		setAppNavigation: (appNavigation: object) => me.set({ appNavigation }),
		setAuthNavigation: (authNavigation: object) => me.set({ authNavigation }),
		navigate(path: string) {
			me.appNavigation.dispatch(resetAction(path))
		},
	}))

function resetAction(routeName: string) {
	return NavigationActions.reset({
		index: 0,
		actions: [NavigationActions.navigate({ routeName })],
	})
}
