import React from 'react'
import _ from 'lodash'
import { reaction, when, useStrict, observable, action, computed } from 'mobx'
import { getRoot, flow, types, getParent, destroy } from 'mobx-state-tree'

import * as u from '../lib/Utils'

useStrict(true)

export const Base = types
	.model({})
	.actions(self => ({
		set: (obj: object) => _.assign(self, obj),
	}))
	.views(self => ({
		get Store() {
			return getRoot(self)
		},
	}))
