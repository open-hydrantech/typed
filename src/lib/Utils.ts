import _ from 'lodash'
import Meteor from 'react-native-meteor'
import * as DeviceInfo from 'react-native-device-info'
import { PermissionsAndroid, AsyncStorage } from 'react-native'
import { IType } from 'mobx-state-tree'

import Settings from '../config/settings'

export const maybeUndefined = <S, T>(type: IType<S, T>): IType<S | undefined, T | undefined> => type

// export const maybeUndefined = <S, T>(type: IType<S, T>) => IType<S | undefined, T | undefined>

// export function modelFactory(model: IModelType<AnyObject, AnyObject>) {
// 	return types.compose(
// 		types
// 			.model({})
// 			.actions(self => ({
// 				set: (obj: object) => _.assign(self, obj),
// 			}))
// 			.views(self => ({
// 				get Store() {
// 					return getRoot(self)
// 				},
// 			})),
// 		model,
// 	)
// }

// Compare props and return modified next props
export function difProps(prevProps: AnyObject, nextProps: AnyObject) {
	return _.reduce(
		nextProps,
		(result: AnyObject, value, key) => {
			if (value !== prevProps[key]) result[key] = true
			return result
		},
		{},
	)
}

export function parseLodash(str: string) {
	return _.attempt(JSON.parse.bind(null, str))
}

export const Server = {
	connect: () =>
		Meteor.connect(Settings.SERVER_URL, {
			autoReconnect: true,
			reconnectInterval: 5000,
		}),
	disconnect: () => Meteor.disconnect(),
	reset: async () => {
		Server.disconnect()
		await sleep(6000)
		Server.connect()
		await sleep(6000)
	},
	connected: () => Meteor.status().connected,
}

export function formatDate(date: Date, isoDate?: Date) {
	if (isoDate) date = new Date(isoDate)
	if (!date) return ''
	const hours = _.padStart(date.getHours().toString(), 2, '0')
	const minutes = _.padStart(date.getMinutes().toString(), 2, '0')
	const seconds = _.padStart(date.getSeconds().toString(), 2, '0')
	const day = _.padStart(date.getDate().toString(), 2, '0')
	const month = _.padStart((date.getMonth() + 1).toString(), 2, '0')
	const year = _.padStart(date.getFullYear().toString(), 2, '0')

	return `${day}.${month}.${year} - ${hours}.${minutes}.${seconds}`
}

export function meteorCall(method: string, params = {}) {
	return new Promise((resolve, reject) => {
		Meteor.call(method, params, (error: any, result: any) => {
			if (error) reject(error)
			resolve(result)
		})
	})
}
export function meteorLoginWithPassword(email: string, password: string) {
	return new Promise((resolve, reject) => {
		Meteor.loginWithPassword(email, password, (error: any, result: any) => {
			if (error) reject(error)
			resolve(result)
		})
	})
}

export function sleep(ms: number) {
	return new Promise(resolve => setTimeout(resolve, ms))
}

type TOPromise = Promise<{
	data: any
	err?: any
	timeOut?: boolean
}>

export function toto(promise: Promise<any>, timeOut?: number): TOPromise {
	let handle: number
	let p = promise
	if (timeOut) {
		p = Promise.race([
			promise,
			new Promise((resolve, reject) => {
				handle = setTimeout(() => {
					reject(new Error('timeOut'))
				}, timeOut)
			}),
		]).then(
			v => {
				clearTimeout(handle)
				return v
			},
			err => {
				clearTimeout(handle)
				throw err
			},
		)
	}
	return p
		.then((data: any) => {
			if (typeof data === 'object') return { data }
			else return { data: {} }
		})
		.catch((err: Error) => {
			if (err.message == 'timeOut') return { data: {}, timeOut: true }
			else return { data: {}, err }
		})
}

export function getCurrentPosition(params = {}) {
	return toto(
		new Promise((resolve, reject) => {
			navigator.geolocation.getCurrentPosition(position => resolve(position), error => reject(error), params)
		}),
	)
}

export function pastDate(days: number) {
	const dateOffset = 24 * 60 * 60 * 1000 // day
	const past = new Date()
	let now = new Date().getTime()
	now = 10000000 * Math.round(now / 10000000)
	past.setTime(now - dateOffset * days) // month
	return past
}

export const getDeviceInfo = () => ({
	uniqueId: DeviceInfo.getUniqueID(),
	manufacturer: DeviceInfo.getManufacturer(),
	brand: DeviceInfo.getBrand(),
	model: DeviceInfo.getModel(),
	deviceId: DeviceInfo.getDeviceId(),
	systemName: DeviceInfo.getSystemName(),
	systemVersion: DeviceInfo.getSystemVersion(),
	bundleId: DeviceInfo.getBundleId(),
	buildNumber: DeviceInfo.getBuildNumber(),
	version: DeviceInfo.getVersion(),
	readableVersion: DeviceInfo.getReadableVersion(),
	deviceName: DeviceInfo.getDeviceName(),
	userAgent: DeviceInfo.getUserAgent(),
	deviceLocale: DeviceInfo.getDeviceLocale(),
	deviceCountry: DeviceInfo.getDeviceCountry(),
	timezone: DeviceInfo.getTimezone(),
	emulator: DeviceInfo.isEmulator(),
	tablet: DeviceInfo.isTablet(),
	pinOrFingerprintSet: DeviceInfo.isPinOrFingerprintSet(),
	apiLevel: DeviceInfo.getAPILevel(),
	instanceId: DeviceInfo.getInstanceID(),
	phoneNumber: DeviceInfo.getPhoneNumber(),
	firstInstallTime: DeviceInfo.getFirstInstallTime(),
	lastUpdateTime: DeviceInfo.getLastUpdateTime(),
})

export const LocalStorage = {
	get: async (key: string) =>
		AsyncStorage.getItem(key)
			.then(value => parseLodash(value))
			.catch(err => console.log(err)),
	save: async (key: string, value: string) => AsyncStorage.setItem(key, JSON.stringify(value)),
	delete: async (key: string) => AsyncStorage.removeItem(key),
	clear: async () => AsyncStorage.clear(),
}

export async function interval(func: () => void, int: number) {
	const enterDate = new Date()
	await func()
	const spent = msPassed(enterDate)
	await sleep(spent > int ? 1000 : int - spent)
	interval(func, int)
}

// const set = (component, obj) => {
// 	return new Promise(resolve => {
// 		const key = _.keys(obj)[0]
// 		const value = _.values(obj)[0]
// 		if (component.state[key] !== value) {
// 			component.setState(obj, () => resolve(obj))
// 		} else {
// 			resolve()
// 		}
// 	})
// }

// export function isNumeric(x:number) {
// 	return (typeof x === 'number' || typeof x === 'string') && !_.isNaN(Number(x)) && x !== ''
// }

export function arrayAssign(array: AnyObject[], newObj: AnyObject) {
	_.forEach(array, obj => _.assign(obj, newObj))
}

// export function createStore(stores = {}, initialData = {}) {
// 	const state = observable({})
// 	const actions = {}

// 	_.forOwn(stores, (store, key) => {
// 		const storeActions = store(state, actions, initialData, key)
// 		actions[key] = storeActions
// 	})

// 	return { state, actions }
// }

export function deferRun(ms: number) {
	return new Promise(resolve => setTimeout(() => requestAnimationFrame(resolve), ms || 100))
}

// export function pif(i) {
// 	return new Promise((resolve, reject) => {
// 		if (i) resolve()
// 		// else reject();
// 	})
// }
export function isNotEmptyString(s: string) {
	return typeof s == 'string' && s != ''
}

export async function requestLocationPermission() {
	try {
		const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
			title: 'גישה למיקום מדוייק',
			message: 'נא אשר',
		})
		if (granted === PermissionsAndroid.RESULTS.GRANTED) {
			console.log('You can use location')
		} else {
			console.log('location permission denied')
		}
	} catch (err) {
		console.warn(err)
	}
}

export const isEmptyObject = (obj: object): boolean => Object.keys(obj).length === 0 && obj.constructor === Object

export const msPassed = (d: Date) => +new Date() - +d
export const dateMinus = (d1: Date, d2: Date) => +d1 - +d2

// export function toto({ promise, timeOut }: { promise: Promise<any>; timeOut?: number }): TOPromise {
// 	let handle: number
// 	let p = promise
// 	if (timeOut) {
// 		p = Promise.race([
// 			promise,
// 			new Promise((resolve, reject) => {
// 				handle = setTimeout(() => {
// 					reject(new Error('timeOut'))
// 				}, timeOut)
// 			}),
// 		]).then(
// 			v => {
// 				clearTimeout(handle)
// 				return v
// 			},
// 			err => {
// 				clearTimeout(handle)
// 				throw err
// 			},
// 		)
// 	}
// 	return p.then((data: any) => (typeof data === 'object' ? data : {})).catch((err: Error) => {
// 		if (err.message == 'timeOut') return { data: {}, timeOut: true }
// 		else return { data: {}, err }
// 	})
// }
